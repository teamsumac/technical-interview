# Question catégorisation de données (5-10 minutes)

Le fichier ``sensor_data.json`` contient les informations reçu lorsqu'on interroge plusieurs capteurs d'humidité installé sur notre machine de test.

Pour faciliter l'interprétation des données par le client, nous voulons regrouper ces valeurs sous 4 catégories.

Voici comment les catégories sont divisées.

* Sec: <10
* Normal: 10 à 16
* Normal Plus: 17 à 21
* Humide: >22

Voici ce que nous te demandons de faire:

1. Parcours le fichier de données, pour chaque capteur extrait les valeurs ``name`` et ``cap``
1. Détermine la catégorie correspondant à la valeur ``cap``
1. Affiche le résultat de la manière suivante:

```
C06-WS11: Normal
C10-WS8: Humide
...

``` 



# Data categorisation question (5-10 minutes)

The ``sensor_data.json`` contains information received when data is fetched from humidity sensors installed on our test machine.

To allow our customers to more easily understand the data, we want to group each humidity values under 4 categories.

Here is how the categories are divided

* Dry: <10
* Normal: 10 to 16
* Normal Plus: 17 to 21
* Wet: > 22

Here is what we want you to do:

1. Read the data file and for each sensor, extract the ``name`` and ``cap`` values
1. Determine the corresponding category using the ```cap``` value
1. Display your results following this example

```
C06-WS11: Normal
C10-WS8: Wet
...

``` 
