# Question REST API (10-15 minutes)
À l'aide des informations d'identification fourni, connect toi à notre machine de test pour obtenir le status global 
de la machine et affiche le résultat en console.

1. Les information d'identification se trouve dans le fichier `login_info.json`

1. Tu devras premièrement faire un POST à /login sur la machine pour obtenir un token d'identification.
    1. Endpoint REST: https://10.254.254.150:5000/login
    1. Paramètre nécessaire: `machineSerial=GI-007-9006`
    1. Assure toi que le "Content-Type" est "application/json"
    
1. Dans la réponse retourné par le login, utilise `access_token` pour les requêtes suivantes.
    1. Le header des requêtes devront contenir la ligne suivante: `Authorization: Bearer {{access_token}}`

1. Obtient le status global de la machine en exécutant un GET sur /machine/status
    1. Endpoint REST: https://10.254.254.150:5000/machine/status
    1. Paramètre nécessaire `machineSerial=GI-007-9006`
    
1. Affiche le résultat JSON obtenu, tu devrais obtenir quelque chose qui ressemble au JSON au bas du README.


# REST API Question (10-15 minutes)
Using the given identification information, connect to our test machine, get the global status of the machine and print
the response in console.

1. The identification information are found in `login_info.json`

1. You will first need to do a POST at /login on our machine. This will provide you with an identification token.
    1. REST Endpoint: https://10.254.254.150:5000/login
    1. Necessary parameter: `machineSerial=GI-007-9006`
    1. Make sure that "Content-Type" is "application/json"
    
1. In the response from /login, use `access_token` for the following requests.
    1. The header of the next request will need to contain `Authorization: Bearer {{access_token}}`
    
1. Obtain the global machine status doing a GET on /machine/status
    1. REST Endpoint: https://10.254.254.150:5000/machine/status
    1. Necessary parameter: `machineSerial=GI-007-9006`
    
1. Print the obtained JSON result, it should look like the JSON bellow. 


### JSON
```
{
  "battery": {
    "charge_level": 100,
    "health": "BAD",
    "model": "lead_acid_12v",
    "power_source_present": true
  },
  "gardenPosition": {
    "harvest": null,
    "injection": 14,
    "nextHarvest": null,
    "nextInjection": 15,
    "nextSpraying": null,
    "spraying": null,
    "state": "INITIALIZED"
  },
  "injection": null,
  "killSwitch": {
    "killSwitchState": "DISENGAGED"
  },
  "lamps": [
    {
      "cooldownRemainingInSec": 0,
      "id": 1,
      "intensity": 100,
      "warmupRemainingInSec": 180
    },
    {
      "cooldownRemainingInSec": 0,
      "id": 2,
      "intensity": 100,
      "warmupRemainingInSec": 179
    }
  ],
  "liquidType": null,
  "machineSerial": "GI-007-9008",
  "rotation": {
    "current": 0.8799446225166321,
    "direction": "COUNTERCLOCKWISE",
    "speedInMinutesPerRotation": 51.10453414916992,
    "voltage": 24.044002313475996
  }
}
```
    

    