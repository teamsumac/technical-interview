# Question State Machine (5-10 minutes)
 
Écrit une state machine qui permettrait de contrôler les lumières de la machine. Cette state machine sera exécuté par la boucle de contrôle principal une fois par seconde.
La state machine devra respecter les critères suivant.

* Une lampe peut varier d'intensité entre 0% et 100%

* Lorsqu'une lampe est éteinte (0%), on doit la laisser refroidir pendant 5 minutes (cooldown) avant d'autoriser l'allumage de la lumière de nouveau.
    * Par exemple, si la lampe est éteinte et que 2 minutes plus tard une commande pour allumé la lampe est reçu, la state machine doit attendre 3 minutes avant d'allumer la lampe de nouveau.
    
* Lorsque la lampe est dans un état stable (allumé ou éteint), la state machine doit confirmer que l'intensité actuel est bien celle demandé. Si ce n'est pas le cas, elle doit ajuster l'intensité.

* Dans ton code, tu peux simplement utiliser une fonction comme get_intensity() ou set_intensity(X) pour contrôler la lampe.    


# State Machine Question (5-10 minutes)

Write a state machine that will allow you to control the lamps on a machine. This state machine will be executed by our control loop once per second.
The state machine need the follow the following criteria.

* A lamp can vary in intensity between 0 and 100

* When a lamp is turned off (0%), we need to let it cool for 5 minutes (cooldown) before allowing a lamp to be open again.
    * For example, if a lamp is turned off and that 2 minutes later a turn on command is received, the state machine should wait 3 minutes before opening the lamp again.
    
* When the lamp is in a stable state (on or off), the state machine should confirm the intensity of the lamp is at the proper level. If this is not the case, the intensity should be adjusted.

* In your code, you can simply use function like get_intensity() or set_intensity(X) to control a lamp.
