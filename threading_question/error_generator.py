import random
import threading

from threading_question.error import Error


class ErrorGenerator(Error):

    def __init__(self, error_file_path: str, error_file_lock: threading.Lock):
        self._error_thread = threading.Thread(target=self._generate_error, daemon=True)
        super().__init__(error_file_path, error_file_lock)

    def get_thread(self) -> threading.Thread:
        return self._error_thread

    def _generate_error(self):
        while self._active:
            # Acquire lock on error file
            with self._error_file_lock:
                # Open error file
                with open(self._error_file_path, "w") as error_file:
                    # Generate random number between 0 and 5
                    error = random.randint(0, 5)
                    # If number is 0 we have an error, write true
                    if error == 0:
                        error_file.write("True")
                    else:
                        error_file.write("False")
            # Wait for 0.2s before monitoring files again
            self._stop_event.wait(timeout=0.2)
