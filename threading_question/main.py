import signal
import threading

from threading_question.error_generator import ErrorGenerator
from threading_question.error_monitor import ErrorMonitor

ERROR_FILE_PATH = "error.txt"


class Main:
    def __init__(self):
        self._stop_event = threading.Event()
        self.error_file_lock = threading.Lock()
        self.error_generator = ErrorGenerator(error_file_path=ERROR_FILE_PATH, error_file_lock=self.error_file_lock)
        self.error_monitor = ErrorMonitor(error_file_path=ERROR_FILE_PATH, error_file_lock=self.error_file_lock)

    def run(self):
        self.error_generator.start()
        self.error_monitor.start()
        self._stop_event.wait()

    def stop(self):
        self.error_generator.stop()
        self.error_monitor.stop()
        self._stop_event.set()


if __name__ == '__main__':
    main = Main()
     
    signal.signal(signal.SIGINT, main.stop)
    signal.signal(signal.SIGTERM, main.stop)

    main.run()
