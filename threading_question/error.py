import abc
import threading


class Error(metaclass=abc.ABCMeta):

    def __init__(self, error_file_path: str, error_file_lock: threading.Lock):
        self._error_file_path = error_file_path
        self._error_file_lock = error_file_lock
        self._active = False
        self._stop_event = threading.Event()

    @abc.abstractmethod
    def get_thread(self) -> threading.Thread:
        raise NotImplementedError()

    def start(self):
        self._active = True
        self.get_thread().start()

    def stop(self):
        self._active = False
        self._stop_event.set()
