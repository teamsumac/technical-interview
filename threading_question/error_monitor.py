import threading
import time

from threading_question.error import Error
from threading_question.time_utils import convert_epoch_to_string


class ErrorMonitor(Error):

    def __init__(self, error_file_path: str, error_file_lock: threading.Lock):
        super().__init__(error_file_path, error_file_lock)
        self._error_thread = threading.Thread(target=self._monitor_error, daemon=True)
        self._local_error_count = 0

    def get_thread(self) -> threading.Thread:
        return self._error_thread

    def _report_error(self):
        current_time = time.time()
        self._local_error_count += 1
        print(f"{convert_epoch_to_string(current_time)}: Error #{self._local_error_count}")

    def _reset_error_count(self):
        self._local_error_count = 0
        # Also reset error_reset.txt to False
        with open("error_reset.txt", "w") as error_reset_file:
            error_reset_file.write("False")

    def _monitor_error(self):
        # Acquire lock on error file
        with self._error_file_lock:
            while self._active:
                # Check if we need to reset the local error count
                with open("error_reset.txt", "r") as error_reset_file:
                    reset_result = error_reset_file.read()
                if "True" in reset_result:
                    # Reset error count
                    self._reset_error_count()

                # Check if we had an error
                with open(self._error_file_path, "r") as error_file:
                    result = error_file.read()
                    if "True" in result:
                        # Report error
                        self._report_error()
                # Wait for 0.1s before monitoring files again
                self._stop_event.wait(timeout=0.1)
