import datetime
from dateutil import parser

TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"


def convert_string_to_epoch(date_string: str) -> float:
    return parser.parse(date_string).timestamp()


def convert_epoch_to_string(epoch: float) -> str:
    return datetime.datetime.utcfromtimestamp(epoch).strftime(TIME_FORMAT)
