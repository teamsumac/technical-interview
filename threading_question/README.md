# Question threading

Le code suivant lance 2 threads.

* Un thread qui génère des errors aléatoirement. (error_generator.py) 
* Un thread qui monitor les errors et détermine si l'erreur doit être affiché. (error_monitor.py)

### Question #1 (5-10 minutes)
Le thread de monitoring n'est actuellement pas en measure de détecter les erreurs.
Trouve ce qui empêche ce thread de fonctionner correctement. L'erreur ce trouve donc dans le fichier error_monitor.py


### Question #2 (10-15 minutes)
Adapte le code en respectant les critères suivant:

* Une erreur est affiché après que 3 erreurs local soit survenu en moins de 10 secondes.
* Chaque erreur subséquente sont affichées directement
* Si le compte d'erreur est remis à zéro (en utilisant le fichier error_reset.txt), il faut de nouveau avoir 3 erreurs en 10 secondes pour afficher des erreurs de nouveau.



# Threading Question

The following code is launching 2 threads.

* A thread that generate error randomly. (error_generator.py)
* A thread that monitors errors and determine if the error should be displayed. (error_monitor.py)

### Question #1 (5-10 minutes)
The monitoring thread is currently unable to detect errors.
Find what is preventing that thread from working properly. The error is in the error_monitor.py file


### Question #2 (10-15 minutes)
Adapt the code to make is follow these criteria

* An error is only displayed after 3 errors were received in less than 10 seconds.
* Every subsequent errors are displayed directly.
* If the error count is reset (using the error_reset.txt file), you need to get another 3 errors in less than 10 seconds before displaying errors again.
